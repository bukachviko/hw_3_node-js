import fsPromise from 'fs/promises';
import path from 'path';

const NewsPostSchema = {
    id: 'Number',
    title: 'String',
    text: 'String',
    createDate: 'Date',
};

const FILEDB = 'FILEDB.json';

class FileDB {
    static async registrSchema(fileName: string, schema: object): Promise<void> {
        try {
            const data = {
                table: [],
                id: 1,
                schema: schema,
            };

            await fsPromise.writeFile(fileName, JSON.stringify(data, null, 2));
            console.log(`Schema registered for ${fileName}`)
        } catch (err) {
            console.error(`Error creating file ${fileName}:`, err);
        }
    }

    static async getTable(tableName: string): Promise<DbService> {

        const fileData = await fsPromise.readFile(path.join(__dirname, tableName), 'utf-8').catch(() => null);

        if (!fileData) {
            await this.registrSchema(path.join(__dirname, tableName), NewsPostSchema);
        }

        return new DbService(tableName);
    }
}

class DbService {
    tableName: string

    constructor(tableName: string) {
        this.tableName = tableName
    }

    async readTable(): Promise<{id: number; table: any, schema: object}> {
            const data = await fsPromise.readFile(path.join(__dirname, this.tableName), 'utf-8');
            return JSON.parse(data)
    }

    async create(title: string, text: string): Promise<{ id: number; title: string; text: string; createDate: string }> {
        try {
            const createDate = (new Date()).toString();
            const currentData = await this.readTable();
            const newPost = {id: currentData.id, title, text, createDate };
            currentData.table.push(newPost);

            currentData.id++;

            await fsPromise.writeFile(path.join(__dirname, this.tableName), JSON.stringify(currentData, null, 2));
            return newPost;

        }catch (err) {
            console.error(`Error creating object`, err);
            throw err
        }
    }


    async getById(id: number): Promise<object | null> {
        try {
            const data = await this.readTable();
            const item = data.table.find((item : {id: number}) => item.id === id);

            return item || null;
        } catch(err) {
            console.error(`Error finding object`, err);
            throw err;
        }
    }

    async update(id: number, newData:{ title?: string, text?: string }): Promise<any> {
        try {
            const data = await this.readTable();
            let updatedItem : any = null;

            data.table.forEach((item : {id: number, title: string, text: string}) => {
                if(item.id === id) {
                    if (newData.title !== undefined) {
                        item.title = newData.title;
                    }
                    if (newData.text !== undefined) {
                        item.text = newData.text;
                    }
                    updatedItem = item;
                    return;
                    }
            });

            await fsPromise.writeFile(path.join(__dirname, this.tableName), JSON.stringify(data, null, 2));
            return updatedItem;

        }catch (e) {
            console.error(`Error item did not found`, e);
            throw e;
        }
    }

    async delete(id:number): Promise<void> {
        try{
            const data = await this.readTable();
            let deletedItemId : number | null = null;

            data.table.forEach((item : {id: number}, index: number) => {
                if (item.id === id) {
                    deletedItemId = id;
                    data.table.splice(index, 1);
                    return;
                }
            })

            await fsPromise.writeFile(path.join(__dirname, this.tableName), JSON.stringify(data, null, 2));

        }catch (e) {
            console.error(`Error item did not found`, e);
            throw e;
        }
    }

    async getAll(){
        const data = await this.readTable();
        return data.table;
    }
}

(async () => {
    try {
        const dbServiceInstance = await FileDB.getTable(FILEDB);
        // const tableData = await dbServiceInstance.getAll();
        // const tableData = await dbServiceInstance.create('A good night', 'go to the bed');
        // const tableData = await dbServiceInstance.getById(3);
        // const tableData = await dbServiceInstance.update(1, {title: 'Hello', text:'We should to go to park'});
        const tableData = await dbServiceInstance.delete(1);

    } catch (err) {
        console.log('Error:', err);
    }
})();

